package interfaces

type Fleet struct {
	ID   int
	Name string
}

type Fleets interface {
	GetAllByIds(ids []int) ([]Fleet, error)
}
