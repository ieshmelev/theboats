package interfaces

import "time"

type CalendarItem struct {
	BoatID           int
	DateFrom, DateTo time.Time
	Available        bool
}

type Calendar interface {
	GetAvailableByDate(date time.Time, boatsIds []int) ([]CalendarItem, error)
	GetNearestAvailableByDate(date time.Time, boatsIds []int) ([]CalendarItem, error)
}
