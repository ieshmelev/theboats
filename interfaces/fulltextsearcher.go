package interfaces

type FullTextSearchItem struct {
	Name string
	Rank float64
}

type FullTextSearchItemsByRank []FullTextSearchItem

func (ftsibr FullTextSearchItemsByRank) Len() int           { return len(ftsibr) }
func (ftsibr FullTextSearchItemsByRank) Swap(i, j int)      { ftsibr[i], ftsibr[j] = ftsibr[j], ftsibr[i] }
func (ftsibr FullTextSearchItemsByRank) Less(i, j int) bool { return ftsibr[i].Rank > ftsibr[j].Rank }

type FullTextSearcher interface {
	FullTextSearh(query string) ([]FullTextSearchItem, error)
}
