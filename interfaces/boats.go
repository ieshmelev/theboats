package interfaces

type Boat struct {
	ID      int
	FleetID int
	ModelID int
}

type Boats interface {
	GetAllByModelIds(ids []int) ([]Boat, error)
}
