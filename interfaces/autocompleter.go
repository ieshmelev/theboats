package interfaces

type Autocompliter interface {
	Autocomplete(query string) ([]string, error)
}
