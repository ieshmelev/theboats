package interfaces

import (
	"encoding/json"
	"time"
)

type SearchItem struct {
	ModelName                  *string
	BuilderName                *string
	FleetName                  *string
	Available                  bool
	NearestAvailablePeriodFrom *time.Time
	NearestAvailablePeriodTo   *time.Time
}

func (si *SearchItem) MarshalJSON() ([]byte, error) {
	s := struct {
		ModelName                  *string `json:"modelName"`
		BuilderName                *string `json:"builderName"`
		FleetName                  *string `json:"fleetName"`
		Available                  bool    `json:"available"`
		NearestAvailablePeriodFrom *string `json:"nearestAvailablePeriodFrom"`
		NearestAvailablePeriodTo   *string `json:"nearestAvailablePeriodTo"`
	}{
		ModelName:   si.ModelName,
		BuilderName: si.BuilderName,
		FleetName:   si.FleetName,
		Available:   si.Available,
	}
	if si.NearestAvailablePeriodFrom != nil {
		s.NearestAvailablePeriodFrom = new(string)
		*s.NearestAvailablePeriodFrom = si.NearestAvailablePeriodFrom.Format("2006-01-02")
	}
	if si.NearestAvailablePeriodTo != nil {
		s.NearestAvailablePeriodTo = new(string)
		*s.NearestAvailablePeriodTo = si.NearestAvailablePeriodTo.Format("2006-01-02")
	}
	return json.Marshal(&s)
}

type Searcher interface {
	Search(query string) ([]SearchItem, error)
}
