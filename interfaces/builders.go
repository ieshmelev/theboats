package interfaces

type Builder struct {
	ID   int
	Name string
}

type Builders interface {
	GetAllByNameStartedWith(query string) ([]Builder, error)
	GetAllByIds(ids []int) ([]Builder, error)
}
