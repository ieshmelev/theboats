package interfaces

type Model struct {
	ID        int
	Name      string
	BuilderID int
}

type Models interface {
	GetAllByNameStartedWith(query string) ([]Model, error)
	GetAllByBuilderIds(builderIds []int) ([]Model, error)
}
