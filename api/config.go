package main

import "os"

const (
	defaultAPIAddr      = ":80"
	defaultPGConnection = "postgres://user:pass@localhost:5432/db?sslmode=disable"
)

type config struct {
	APIAddr      string
	PGConnection string
}

func newConfig() *config {
	cfg := &config{
		APIAddr:      defaultAPIAddr,
		PGConnection: defaultPGConnection,
	}

	if apiAddr := os.Getenv("API_ADDR"); apiAddr != "" {
		cfg.APIAddr = apiAddr
	}

	if pgConnection := os.Getenv("PG_CONNECTION"); pgConnection != "" {
		cfg.PGConnection = pgConnection
	}

	return cfg
}
