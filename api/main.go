package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"database/sql"

	"bitbucket.org/ieshmelev/theboats/services/autocomplete"
	"bitbucket.org/ieshmelev/theboats/services/pgboats"
	"bitbucket.org/ieshmelev/theboats/services/pgcalendar"
	"bitbucket.org/ieshmelev/theboats/services/pgfleets"
	"bitbucket.org/ieshmelev/theboats/services/search"
	"bitbucket.org/ieshmelev/theboats/services/pgyachtbuilders"
	"bitbucket.org/ieshmelev/theboats/services/pgyachtmodels"
	_ "github.com/lib/pq"
)

func buildDependencies() (*log.Logger, *sql.DB, *api, error) {
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)

	cfg := newConfig()

	db, err := sql.Open("postgres", cfg.PGConnection)
	if err != nil {
		return logger, nil, nil, err
	}
	if err := db.Ping(); err != nil {
		return logger, nil, nil, err
	}

	yachtmodelsService := pgyachtmodels.New(db, logger)
	yachtbuildersService := pgyachtbuilders.New(db, logger)
	boatsService := pgboats.New(db, logger)
	fleetsService := pgfleets.New(db, logger)
	calendarService := pgcalendar.New(db, logger)
	autocompleteService := autocomplete.New(yachtmodelsService, yachtbuildersService)
	searchService := search.New(
		yachtmodelsService,
		yachtbuildersService,
		boatsService,
		fleetsService,
		calendarService,
	)

	return logger, db, newAPI(cfg.APIAddr, logger, autocompleteService, searchService), nil
}

func main() {
	logger, db, a, err := buildDependencies()
	if err != nil {
		logger.Fatal(err)
	}
	defer func() {
		if err := db.Close(); err != nil {
			logger.Println(err)
		}
	}()

	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		logger.Println("running api...")
		if err := a.run(ctx); err != nil {
			logger.Println("api runtime error", err)
			cancel()
		}
	}()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	wg.Add(1)
	go func() {
		defer wg.Done()
		select {
		case <-sigs:
			cancel()
		case <-ctx.Done():
		}
	}()

	wg.Wait()
}
