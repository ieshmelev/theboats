package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/ieshmelev/theboats/interfaces"
)

type api struct {
	srv           *http.Server
	logger        *log.Logger
	autocompliter interfaces.Autocompliter
	searcher      interfaces.Searcher
}

func newAPI(
	addr string,
	logger *log.Logger,
	autocompliter interfaces.Autocompliter,
	searcher interfaces.Searcher,
) *api {
	a := &api{
		logger:        logger,
		autocompliter: autocompliter,
		searcher:      searcher,
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/search", a.search)
	mux.HandleFunc("/autocomplete", a.autocomplete)
	a.srv = &http.Server{
		Addr:    addr,
		Handler: mux,
	}

	return a
}

func (a *api) run(ctx context.Context) error {
	go func() {
		<-ctx.Done()
		a.srv.Close()
	}()

	if err := a.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		return err
	}
	return nil
}

func (a *api) search(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("query")
	if query == "" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "query is required")
		return
	}

	result, err := a.searcher.Search(query)
	if err != nil {
		a.logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(w, "internal error")
		return
	}

	marshaledResult, err := json.Marshal(result)
	if err != nil {
		a.logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(w, "internal error")
		return
	}

	fmt.Fprintln(w, string(marshaledResult))
}

func (a *api) autocomplete(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("query")
	if query == "" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "query is required")
		return
	}

	result, err := a.autocompliter.Autocomplete(query)
	if err != nil {
		a.logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(w, "internal error")
		return
	}

	marshaledResult, err := json.Marshal(result)
	if err != nil {
		a.logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(w, "internal error")
		return
	}

	fmt.Fprintln(w, string(marshaledResult))
}
