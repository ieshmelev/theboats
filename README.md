# theboats

## preparing

### build

```bash
$ docker build --tag theboats:latest .
```

### migrations

Прежде всего нужно создать базу данных на postgres:12.1

```bash
$ docker run --rm \
-e PG_CONNECTION=postgres://user:pass@localhost:5432/db \
theboats:latest /go/bin/migrations
```
В случае возникновения ошбики `pg: SSL is not enabled on the server`, необходимо добавить в PG_CONNECTION `?sslmode=disable`

### update data

```
$ docker run --rm \
--mount type=bind,src=/path/to/files,dst=/usr/src/data,readonly \
-e PG_CONNECTION=postgres://user:pass@localhost:5432/db \
theboats:latest /go/bin/updater
```
В случае возникновения ошбики `pg: SSL is not enabled on the server`, необходимо добавить в PG_CONNECTION `?sslmode=disable`

## running

```bash
$ docker run --rm -d -p PORT:80 \
-e PG_CONNECTION=postgres://user:pass@localhost:5432/db \
theboats:latest /go/bin/api
```
В случае возникновения ошбики `pg: SSL is not enabled on the server`, необходимо добавить в PG_CONNECTION `?sslmode=disable`

```bash
$ curl -X GET http://localhost:PORT/search?query=Cyclades+43.4
$ curl -X GET http://localhost:PORT/autocomplete?query=Cyclades+43.4
```
