CREATE INDEX boats_model_id_idx ON boats (model_id);

-- NOTE: При текущих объемах данных таблицы планировщик предпочитает не использовать данный индекс
-- и вместо него использует seq scan, в отличии от индекса yacht_models_lower_idx.
-- Индекс создан с прицелом на будущее.
CREATE INDEX yacht_builders_lower_idx ON yacht_builders (lower(name) varchar_pattern_ops);

CREATE INDEX yacht_models_lower_idx ON yacht_models (lower(name) varchar_pattern_ops);

CREATE INDEX yacht_models_builder_id_idx ON yacht_models (builder_id);
