CREATE INDEX yacht_models_to_tsvector_idx ON yacht_models USING GIN (to_tsvector('english', name));

CREATE INDEX yacht_builders_to_tsvector_idx ON yacht_builders USING GIN (to_tsvector('english', name));
