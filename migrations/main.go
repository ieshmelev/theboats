package main

import (
	"database/sql"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
	"log"
	"os"
)

func buildDependencies() (*log.Logger, *sql.DB, *migrate.Migrate, error) {
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)

	cfg := newConfig()

	db, err := sql.Open("postgres", cfg.PGConnection)
	if err != nil {
		return logger, nil, nil, err
	}
	if err := db.Ping(); err != nil {
		return logger, nil, nil, err
	}

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return logger, nil, nil, err
	}
	m, err := migrate.NewWithDatabaseInstance(cfg.MigrationsPath, "postgres", driver)

	return logger, db, m, nil
}

func main() {
	logger, db, m, err := buildDependencies()
	if err != nil {
		logger.Fatal(err)
	}
	defer func() {
		srcErr, dbErr := m.Close()
		if srcErr != nil {
			logger.Println(srcErr)
		}
		if dbErr != nil {
			logger.Println(dbErr)
		}
		if err := db.Close(); err != nil {
			logger.Println(err)
		}
	}()
	if err := m.Up(); err != nil {
		logger.Println(err)
	}
}
