package main

import "os"

const (
	defaultPGConnection   = "postgres://user:pass@localhost:5432/db?sslmode=disable"
	defaultMigrationsPath = "file:///usr/src/migrations"
)

type config struct {
	PGConnection   string
	MigrationsPath string
}

func newConfig() *config {
	cfg := &config{
		PGConnection:   defaultPGConnection,
		MigrationsPath: defaultMigrationsPath,
	}

	if pgConnection := os.Getenv("PG_CONNECTION"); pgConnection != "" {
		cfg.PGConnection = pgConnection
	}

	if migrationsPath := os.Getenv("MIGRATIONS_PATH"); migrationsPath != "" {
		cfg.MigrationsPath = migrationsPath
	}

	return cfg
}
