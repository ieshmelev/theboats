CREATE TABLE yacht_builders(
    id INTEGER PRIMARY KEY,
    name VARCHAR
);

CREATE TABLE yacht_models(
    id INTEGER PRIMARY KEY,
    name VARCHAR,
    builder_id INTEGER REFERENCES yacht_builders (id)
);

CREATE TABLE fleets(
    id INTEGER PRIMARY KEY,
    name VARCHAR
);

CREATE TABLE boats(
    id INTEGER PRIMARY KEY,
    fleet_id INTEGER REFERENCES fleets (id),
    model_id INTEGER REFERENCES yacht_models (id)
);

CREATE TABLE calendar(
    boat_id INTEGER REFERENCES boats (id),
    date_from DATE,
    date_to DATE,
    available BOOLEAN
);
