FROM golang:1.12.13 as builder
WORKDIR /usr/src
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o a ./api
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o m ./migrations
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o u ./updater

FROM scratch
COPY --from=builder /usr/src/a /go/bin/api
COPY --from=builder /usr/src/m /go/bin/migrations
COPY --from=builder /usr/src/u /go/bin/updater
COPY --from=builder /usr/src/migrations /usr/src/migrations
EXPOSE 80
