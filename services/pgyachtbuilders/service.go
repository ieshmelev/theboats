package pgyachtbuilders

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	"bitbucket.org/ieshmelev/theboats/interfaces"
)

type Service struct {
	db     *sql.DB
	logger *log.Logger
}

func New(db *sql.DB, logger *log.Logger) *Service {
	return &Service{
		db:     db,
		logger: logger,
	}
}

func (s *Service) FullTextSearh(query string) ([]interfaces.FullTextSearchItem, error) {
	rows, err := s.db.Query(
		`SELECT
			name,
			ts_rank(setweight(to_tsvector('english', name), 'A'), plainto_tsquery($1)) AS rank
		FROM yacht_builders
		WHERE to_tsvector('english', name) @@ plainto_tsquery($1)
		ORDER BY rank DESC`,
		query,
	)
	if err != nil {
		return nil, fmt.Errorf("yachtbuilders FullTextSearh query error: %s", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			s.logger.Println(err)
		}
	}()

	result := []interfaces.FullTextSearchItem{}
	for rows.Next() {
		name, rank := "", 0.0
		if err := rows.Scan(&name, &rank); err != nil {
			return nil, fmt.Errorf("yachtbuilders FullTextSearh scan row error: %s", err)
		}
		result = append(result, interfaces.FullTextSearchItem{
			Name: name,
			Rank: rank,
		})
	}

	return result, nil
}

func (s *Service) GetAllByNameStartedWith(query string) ([]interfaces.Builder, error) {
	rows, err := s.db.Query(
		"SELECT * FROM yacht_builders WHERE lower(name) LIKE lower($1)",
		fmt.Sprintf("%s%%", query),
	)
	if err != nil {
		return nil, fmt.Errorf("yachtbuilders GetAllByNameStartedWith query error: %s", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			s.logger.Println(err)
		}
	}()

	result := []interfaces.Builder{}
	for rows.Next() {
		id, name := 0, ""
		if err := rows.Scan(&id, &name); err != nil {
			return nil, fmt.Errorf("yachtbuilders GetAllByNameStartedWith scan row error: %s", err)
		}
		result = append(result, interfaces.Builder{
			ID:   id,
			Name: name,
		})
	}

	return result, nil
}

func (s *Service) GetAllByIds(ids []int) ([]interfaces.Builder, error) {
	idsLen := len(ids)
	if idsLen == 0 {
		return nil, nil
	}
	whereParts := make([]string, idsLen)
	args := make([]interface{}, idsLen)
	for k, v := range ids {
		whereParts[k] = fmt.Sprintf("$%d", k+1)
		args[k] = v
	}
	rows, err := s.db.Query(
		fmt.Sprintf("SELECT * FROM yacht_builders WHERE id IN (%s)", strings.Join(whereParts, ",")),
		args...,
	)
	if err != nil {
		return nil, fmt.Errorf("yachtbuilders GetAllByIds query error: %s", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			s.logger.Println(err)
		}
	}()

	result := []interfaces.Builder{}
	for rows.Next() {
		id, name := 0, ""
		if err := rows.Scan(&id, &name); err != nil {
			return nil, fmt.Errorf("yachtbuilders GetAllByIds scan row error: %s", err)
		}
		result = append(result, interfaces.Builder{
			ID:   id,
			Name: name,
		})
	}

	return result, nil
}
