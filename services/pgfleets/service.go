package pgfleets

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	"bitbucket.org/ieshmelev/theboats/interfaces"
)

type Service struct {
	db     *sql.DB
	logger *log.Logger
}

func New(db *sql.DB, logger *log.Logger) *Service {
	return &Service{
		db:     db,
		logger: logger,
	}
}

func (s *Service) GetAllByIds(ids []int) ([]interfaces.Fleet, error) {
	idsLen := len(ids)
	if idsLen == 0 {
		return nil, nil
	}
	whereParts := make([]string, idsLen)
	args := make([]interface{}, idsLen)
	for k, v := range ids {
		whereParts[k] = fmt.Sprintf("$%d", k+1)
		args[k] = v
	}
	rows, err := s.db.Query(
		fmt.Sprintf("SELECT * FROM fleets WHERE id IN (%s)", strings.Join(whereParts, ",")),
		args...,
	)
	if err != nil {
		return nil, fmt.Errorf("fleets GetAllByIds query error: %s", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			s.logger.Println(err)
		}
	}()

	result := []interfaces.Fleet{}
	for rows.Next() {
		id, name := 0, ""
		if err := rows.Scan(&id, &name); err != nil {
			return nil, fmt.Errorf("fleets GetAllByIds scan row error: %s", err)
		}
		result = append(result, interfaces.Fleet{
			ID:   id,
			Name: name,
		})
	}

	return result, nil
}
