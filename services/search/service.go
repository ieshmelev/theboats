package search

import (
	"sync"
	"time"

	"bitbucket.org/ieshmelev/theboats/interfaces"
)

type Service struct {
	models   interfaces.Models
	builders interfaces.Builders
	boats    interfaces.Boats
	fleets   interfaces.Fleets
	calendar interfaces.Calendar
}

func New(
	models interfaces.Models,
	builders interfaces.Builders,
	boats interfaces.Boats,
	fleets interfaces.Fleets,
	calendar interfaces.Calendar,
) *Service {
	return &Service{
		models:   models,
		builders: builders,
		boats:    boats,
		fleets:   fleets,
		calendar: calendar,
	}
}

// NOTE: метод выглядит достаточно монструозно и в принципе задачу поиска можно решить
// одним сложным запросом или с помощью мат вью, но я предпочитаю данный метод, так как он дает дополнительное
// пространство для масштабирования.
func (s *Service) Search(query string) ([]interfaces.SearchItem, error) {

	wg := sync.WaitGroup{}

	var models []interfaces.Model
	var modelsErr error
	var modelsIds []int
	modelsMap := map[int]interfaces.Model{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		models, modelsErr = s.models.GetAllByNameStartedWith(query)
		if modelsErr != nil {
			return
		}
		modelsIds = make([]int, len(models))
		for k, v := range models {
			modelsIds[k] = v.ID
			modelsMap[v.ID] = v
		}
	}()

	var builders []interfaces.Builder
	var buildersErr error
	var buildersIds []int
	buildersMap := map[int]interfaces.Builder{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		builders, buildersErr = s.builders.GetAllByNameStartedWith(query)
		if buildersErr != nil {
			return
		}
		buildersIds = make([]int, len(builders))
		for k, v := range builders {
			buildersIds[k] = v.ID
			buildersMap[v.ID] = v
		}
	}()

	wg.Wait()

	if modelsErr != nil {
		return nil, modelsErr
	}
	if buildersErr != nil {
		return nil, buildersErr
	}

	missingBuildersIds := []int{}
	for _, v := range models {
		if _, ok := buildersMap[v.BuilderID]; !ok {
			missingBuildersIds = append(missingBuildersIds, v.BuilderID)
		}
	}

	wg.Add(1)
	go func() {
		defer wg.Done()
		var additionalModels []interfaces.Model
		additionalModels, modelsErr = s.models.GetAllByBuilderIds(buildersIds)
		if modelsErr != nil {
			return
		}
		modelsLen := len(models) + len(additionalModels)
		modelsIds = append(make([]int, 0, modelsLen), modelsIds...)
		for _, v := range additionalModels {
			if _, ok := modelsMap[v.ID]; !ok {
				modelsIds = append(modelsIds, v.ID)
				modelsMap[v.ID] = v
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		var additionalBuilders []interfaces.Builder
		additionalBuilders, buildersErr = s.builders.GetAllByIds(missingBuildersIds)
		if modelsErr != nil {
			return
		}
		for _, v := range additionalBuilders {
			buildersMap[v.ID] = v
		}
	}()

	wg.Wait()

	if modelsErr != nil {
		return nil, modelsErr
	}
	if buildersErr != nil {
		return nil, buildersErr
	}

	boats, err := s.boats.GetAllByModelIds(modelsIds)
	if err != nil {
		return nil, err
	}
	boatsIds := make([]int, len(boats))
	for k, v := range boats {
		boatsIds[k] = v.ID
	}

	var fleetsErr error
	fleetsMap := map[int]interfaces.Fleet{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		missingFleetsIds := make([]int, 0, len(boats))
		missingFleetsMap := map[int]struct{}{}
		for _, v := range boats {
			if _, ok := missingFleetsMap[v.FleetID]; !ok {
				missingFleetsIds = append(missingFleetsIds, v.FleetID)
				missingFleetsMap[v.FleetID] = struct{}{}
			}
		}
		var fleets []interfaces.Fleet
		fleets, fleetsErr = s.fleets.GetAllByIds(missingFleetsIds)
		if fleetsErr != nil {
			return
		}
		for _, v := range fleets {
			fleetsMap[v.ID] = v
		}
	}()

	now := time.Now()
	var avaliableErr error
	avaliableMap := map[int]interfaces.CalendarItem{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		var avaliable []interfaces.CalendarItem
		avaliable, avaliableErr = s.calendar.GetAvailableByDate(now, boatsIds)
		if avaliableErr != nil {
			return
		}
		for _, v := range avaliable {
			avaliableMap[v.BoatID] = v
		}
	}()

	var nearestAvaliableErr error
	nearestAvaliableMap := map[int]interfaces.CalendarItem{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		var avaliable []interfaces.CalendarItem
		avaliable, nearestAvaliableErr = s.calendar.GetNearestAvailableByDate(now, boatsIds)
		if nearestAvaliableErr != nil {
			return
		}
		for _, v := range avaliable {
			nearestAvaliableMap[v.BoatID] = v
		}
	}()

	wg.Wait()

	if fleetsErr != nil {
		return nil, fleetsErr
	}
	if avaliableErr != nil {
		return nil, avaliableErr
	}
	if nearestAvaliableErr != nil {
		return nil, nearestAvaliableErr
	}

	result := make([]interfaces.SearchItem, len(boats))
	for k, v := range boats {
		item := interfaces.SearchItem{
			Available: avaliableMap[v.ID].Available,
		}
		if model, ok := modelsMap[v.ModelID]; ok {
			item.ModelName = &model.Name
			if builder, ok := buildersMap[model.BuilderID]; ok {
				item.BuilderName = &builder.Name
			}
		}
		if fleet, ok := fleetsMap[v.FleetID]; ok {
			item.FleetName = &fleet.Name
		}
		if a, ok := nearestAvaliableMap[v.ID]; ok {
			item.NearestAvailablePeriodFrom = &a.DateFrom
			item.NearestAvailablePeriodTo = &a.DateTo
		}
		result[k] = item
	}
	return result, nil
}
