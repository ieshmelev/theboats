package search

import (
	"fmt"
	"testing"
	"time"

	"bitbucket.org/ieshmelev/theboats/interfaces"
	"gotest.tools/assert"
)

type stubSuccessModels struct{}

func (s *stubSuccessModels) GetAllByNameStartedWith(query string) ([]interfaces.Model, error) {
	return []interfaces.Model{interfaces.Model{1, "first model", 3}}, nil
}
func (s *stubSuccessModels) GetAllByBuilderIds(builderIds []int) ([]interfaces.Model, error) {
	return []interfaces.Model{interfaces.Model{2, "second model", 4}}, nil
}

var errStubFirstFailedsModels = fmt.Errorf("errStubFirstFailedsModels")

type stubFirstFailedsModels struct{}

func (s *stubFirstFailedsModels) GetAllByNameStartedWith(query string) ([]interfaces.Model, error) {
	return nil, errStubFirstFailedsModels
}
func (s *stubFirstFailedsModels) GetAllByBuilderIds(builderIds []int) ([]interfaces.Model, error) {
	return nil, nil
}

var errStubSecondFailedsModels = fmt.Errorf("errStubSecondFailedsModels")

type stubSecondFailedsModels struct{}

func (s *stubSecondFailedsModels) GetAllByNameStartedWith(query string) ([]interfaces.Model, error) {
	return nil, nil
}
func (s *stubSecondFailedsModels) GetAllByBuilderIds(builderIds []int) ([]interfaces.Model, error) {
	return nil, errStubSecondFailedsModels
}

type stubSuccessBuilders struct{}

func (s *stubSuccessBuilders) GetAllByNameStartedWith(query string) ([]interfaces.Builder, error) {
	return []interfaces.Builder{interfaces.Builder{4, "fourth builder"}}, nil
}
func (s *stubSuccessBuilders) GetAllByIds(ids []int) ([]interfaces.Builder, error) {
	return []interfaces.Builder{interfaces.Builder{3, "third builder"}}, nil
}

var errStubFirstFailedBuilders = fmt.Errorf("errStubFirstFailedBuilders")

type stubFirstFailedBuilders struct{}

func (s *stubFirstFailedBuilders) GetAllByNameStartedWith(query string) ([]interfaces.Builder, error) {
	return nil, errStubFirstFailedBuilders
}
func (s *stubFirstFailedBuilders) GetAllByIds(ids []int) ([]interfaces.Builder, error) {
	return nil, nil
}

var errStubSecondFailedBuilders = fmt.Errorf("errStubSecondFailedBuilders")

type stubSecondFailedBuilders struct{}

func (s *stubSecondFailedBuilders) GetAllByNameStartedWith(query string) ([]interfaces.Builder, error) {
	return nil, nil
}
func (s *stubSecondFailedBuilders) GetAllByIds(ids []int) ([]interfaces.Builder, error) {
	return nil, errStubSecondFailedBuilders
}

type stubSuccessBoats struct{}

func (s *stubSuccessBoats) GetAllByModelIds(ids []int) ([]interfaces.Boat, error) {
	return []interfaces.Boat{interfaces.Boat{5, 7, 1}, interfaces.Boat{6, 8, 2}}, nil
}

var errStubFailedBoats = fmt.Errorf("errStubFailedBoats")

type stubFailedBoats struct{}

func (s *stubFailedBoats) GetAllByModelIds(ids []int) ([]interfaces.Boat, error) {
	return nil, errStubFailedBoats
}

type stubSuccessFleets struct{}

func (s *stubSuccessFleets) GetAllByIds(ids []int) ([]interfaces.Fleet, error) {
	return []interfaces.Fleet{interfaces.Fleet{7, "seventh fleet"}, interfaces.Fleet{8, "eighth fleet"}}, nil
}

var errStubFailedFleets = fmt.Errorf("errStubFailedFleets")

type stubFailedFleets struct{}

func (s *stubFailedFleets) GetAllByIds(ids []int) ([]interfaces.Fleet, error) {
	return nil, errStubFailedFleets
}

type stubSuccessCalendar struct{}

func (s *stubSuccessCalendar) GetAvailableByDate(date time.Time, boatsIds []int) ([]interfaces.CalendarItem, error) {
	return []interfaces.CalendarItem{
		interfaces.CalendarItem{
			BoatID:    5,
			DateFrom:  time.Date(2019, 1, 1, 0, 0, 0, 0, &time.Location{}),
			DateTo:    time.Date(2019, 1, 31, 0, 0, 0, 0, &time.Location{}),
			Available: true,
		},
	}, nil
}
func (s *stubSuccessCalendar) GetNearestAvailableByDate(date time.Time, boatsIds []int) ([]interfaces.CalendarItem, error) {
	return []interfaces.CalendarItem{
		interfaces.CalendarItem{
			BoatID:    5,
			DateFrom:  time.Date(2019, 1, 1, 0, 0, 0, 0, &time.Location{}),
			DateTo:    time.Date(2019, 1, 31, 0, 0, 0, 0, &time.Location{}),
			Available: true,
		},
		interfaces.CalendarItem{
			BoatID:    6,
			DateFrom:  time.Date(2019, 2, 1, 0, 0, 0, 0, &time.Location{}),
			DateTo:    time.Date(2019, 2, 28, 0, 0, 0, 0, &time.Location{}),
			Available: true,
		},
	}, nil
}

var errStubFirstFailedCalendar = fmt.Errorf("errStubFirstFailedCalendar")

type stubFirstFailedCalendar struct{}

func (s *stubFirstFailedCalendar) GetAvailableByDate(date time.Time, boatsIds []int) ([]interfaces.CalendarItem, error) {
	return nil, errStubFirstFailedCalendar
}
func (s *stubFirstFailedCalendar) GetNearestAvailableByDate(date time.Time, boatsIds []int) ([]interfaces.CalendarItem, error) {
	return nil, nil
}

var errStubSecondFailedCalendar = fmt.Errorf("errStubSecondFailedCalendar")

type stubSecondFailedCalendar struct{}

func (s *stubSecondFailedCalendar) GetAvailableByDate(date time.Time, boatsIds []int) ([]interfaces.CalendarItem, error) {
	return nil, nil
}
func (s *stubSecondFailedCalendar) GetNearestAvailableByDate(date time.Time, boatsIds []int) ([]interfaces.CalendarItem, error) {
	return nil, errStubSecondFailedCalendar
}

func TestSuccess(t *testing.T) {
	firstModel := new(string)
	*firstModel = "first model"
	secondModel := new(string)
	*secondModel = "second model"
	thirdBilder := new(string)
	*thirdBilder = "third builder"
	fourthBuilder := new(string)
	*fourthBuilder = "fourth builder"
	seventhFleet := new(string)
	*seventhFleet = "seventh fleet"
	eighthFleet := new(string)
	*eighthFleet = "eighth fleet"
	t1 := &time.Time{}
	*t1 = time.Date(2019, 1, 1, 0, 0, 0, 0, &time.Location{})
	t2 := &time.Time{}
	*t2 = time.Date(2019, 1, 31, 0, 0, 0, 0, &time.Location{})
	t3 := &time.Time{}
	*t3 = time.Date(2019, 2, 1, 0, 0, 0, 0, &time.Location{})
	t4 := &time.Time{}
	*t4 = time.Date(2019, 2, 28, 0, 0, 0, 0, &time.Location{})
	expected := []interfaces.SearchItem{
		interfaces.SearchItem{
			ModelName:                  firstModel,
			BuilderName:                thirdBilder,
			FleetName:                  seventhFleet,
			Available:                  true,
			NearestAvailablePeriodFrom: t1,
			NearestAvailablePeriodTo:   t2,
		},
		interfaces.SearchItem{
			ModelName:                  secondModel,
			BuilderName:                fourthBuilder,
			FleetName:                  eighthFleet,
			Available:                  false,
			NearestAvailablePeriodFrom: t3,
			NearestAvailablePeriodTo:   t4,
		},
	}
	s := New(
		&stubSuccessModels{},
		&stubSuccessBuilders{},
		&stubSuccessBoats{},
		&stubSuccessFleets{},
		&stubSuccessCalendar{},
	)
	actual, err := s.Search("")
	assert.Equal(t, nil, err)
	assert.DeepEqual(t, actual, expected)
}

func TestFailed(t *testing.T) {
	var expected []interfaces.SearchItem

	s := New(
		&stubFirstFailedsModels{},
		&stubFirstFailedBuilders{},
		&stubFailedBoats{},
		&stubFailedFleets{},
		&stubFirstFailedCalendar{},
	)
	actual, err := s.Search("")
	assert.Equal(t, errStubFirstFailedsModels, err)
	assert.DeepEqual(t, actual, expected)

	s = New(
		&stubSecondFailedsModels{},
		&stubFirstFailedBuilders{},
		&stubFailedBoats{},
		&stubFailedFleets{},
		&stubFirstFailedCalendar{},
	)
	actual, err = s.Search("")
	assert.Equal(t, errStubFirstFailedBuilders, err)
	assert.DeepEqual(t, actual, expected)

	s = New(
		&stubSecondFailedsModels{},
		&stubSecondFailedBuilders{},
		&stubFailedBoats{},
		&stubFailedFleets{},
		&stubFirstFailedCalendar{},
	)
	actual, err = s.Search("")
	assert.Equal(t, errStubSecondFailedsModels, err)
	assert.DeepEqual(t, actual, expected)

	s = New(
		&stubSuccessModels{},
		&stubSecondFailedBuilders{},
		&stubFailedBoats{},
		&stubFailedFleets{},
		&stubFirstFailedCalendar{},
	)
	actual, err = s.Search("")
	assert.Equal(t, errStubSecondFailedBuilders, err)
	assert.DeepEqual(t, actual, expected)

	s = New(
		&stubSuccessModels{},
		&stubSuccessBuilders{},
		&stubFailedBoats{},
		&stubFailedFleets{},
		&stubFirstFailedCalendar{},
	)
	actual, err = s.Search("")
	assert.Equal(t, errStubFailedBoats, err)
	assert.DeepEqual(t, actual, expected)

	s = New(
		&stubSuccessModels{},
		&stubSuccessBuilders{},
		&stubSuccessBoats{},
		&stubFailedFleets{},
		&stubFirstFailedCalendar{},
	)
	actual, err = s.Search("")
	assert.Equal(t, errStubFailedFleets, err)
	assert.DeepEqual(t, actual, expected)

	s = New(
		&stubSuccessModels{},
		&stubSuccessBuilders{},
		&stubSuccessBoats{},
		&stubSuccessFleets{},
		&stubFirstFailedCalendar{},
	)
	actual, err = s.Search("")
	assert.Equal(t, errStubFirstFailedCalendar, err)
	assert.DeepEqual(t, actual, expected)

	s = New(
		&stubSuccessModels{},
		&stubSuccessBuilders{},
		&stubSuccessBoats{},
		&stubSuccessFleets{},
		&stubSecondFailedCalendar{},
	)
	actual, err = s.Search("")
	assert.Equal(t, errStubSecondFailedCalendar, err)
	assert.DeepEqual(t, actual, expected)
}
