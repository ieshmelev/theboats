package pgcalendar

import (
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	"bitbucket.org/ieshmelev/theboats/interfaces"
)

type Service struct {
	db     *sql.DB
	logger *log.Logger
}

func New(db *sql.DB, logger *log.Logger) *Service {
	return &Service{
		db:     db,
		logger: logger,
	}
}

func (s *Service) GetAvailableByDate(date time.Time, boatsIds []int) ([]interfaces.CalendarItem, error) {
	boatsIdsLen := len(boatsIds)
	if boatsIdsLen == 0 {
		return nil, nil
	}
	whereParts := make([]string, boatsIdsLen)
	args := make([]interface{}, boatsIdsLen+1)
	args[0] = date
	for k, v := range boatsIds {
		whereParts[k] = fmt.Sprintf("$%d", k+2)
		args[k+1] = v
	}
	rows, err := s.db.Query(
		fmt.Sprintf(
			`SELECT *
			FROM calendar
			WHERE
				boat_id IN (%s)
				AND $1 BETWEEN date_from AND date_to
				AND available = true
			`,
			strings.Join(whereParts, ","),
		),
		args...,
	)
	if err != nil {
		return nil, fmt.Errorf("calendar GetAvailableByDate query error: %s", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			s.logger.Println(err)
		}
	}()

	result := []interfaces.CalendarItem{}
	for rows.Next() {
		boatID, dateFrom, dateTo, avaliable := 0, time.Time{}, time.Time{}, false
		if err := rows.Scan(&boatID, &dateFrom, &dateTo, &avaliable); err != nil {
			return nil, fmt.Errorf("calendar GetAvailableByDate scan row error: %s", err)
		}
		result = append(result, interfaces.CalendarItem{
			BoatID:    boatID,
			DateFrom:  dateFrom,
			DateTo:    dateTo,
			Available: avaliable,
		})
	}

	return result, nil
}

func (s *Service) GetNearestAvailableByDate(
	date time.Time,
	boatsIds []int,
) ([]interfaces.CalendarItem, error) {
	boatsIdsLen := len(boatsIds)
	if boatsIdsLen == 0 {
		return nil, nil
	}
	whereParts := make([]string, boatsIdsLen)
	args := make([]interface{}, boatsIdsLen+1)
	args[0] = date
	for k, v := range boatsIds {
		whereParts[k] = fmt.Sprintf("$%d", k+2)
		args[k+1] = v
	}
	rows, err := s.db.Query(
		fmt.Sprintf(
			`SELECT boat_id, date_from, date_to, available
			FROM (
				SELECT *, ROW_NUMBER() OVER (PARTITION BY boat_id ORDER BY date_from) num
				FROM calendar
				WHERE
					boat_id IN (%s)
					AND $1 <= date_to
					AND available = true
			) t
			WHERE
				num = 1
			`,
			strings.Join(whereParts, ","),
		),
		args...,
	)
	if err != nil {
		return nil, fmt.Errorf("calendar GetNearestAvailableByDate query error: %s", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			s.logger.Println(err)
		}
	}()

	result := []interfaces.CalendarItem{}
	for rows.Next() {
		boatID, dateFrom, dateTo, avaliable := 0, time.Time{}, time.Time{}, false
		if err := rows.Scan(&boatID, &dateFrom, &dateTo, &avaliable); err != nil {
			return nil, fmt.Errorf("calendar GetNearestAvailableByDate scan row error: %s", err)
		}
		result = append(result, interfaces.CalendarItem{
			BoatID:    boatID,
			DateFrom:  dateFrom,
			DateTo:    dateTo,
			Available: avaliable,
		})
	}

	return result, nil
}
