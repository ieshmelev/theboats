package autocomplete

import (
	"fmt"
	"testing"

	"bitbucket.org/ieshmelev/theboats/interfaces"
	"gotest.tools/assert"
)

var errExpected = fmt.Errorf("some error")

type stubFirstSuccessAutocompliter struct{}

func (s *stubFirstSuccessAutocompliter) FullTextSearh(query string) ([]interfaces.FullTextSearchItem, error) {
	return []interfaces.FullTextSearchItem{
		interfaces.FullTextSearchItem{"a", 1},
		interfaces.FullTextSearchItem{"c", 3},
		interfaces.FullTextSearchItem{"a", 1},
	}, nil
}

type stubSecondSuccessAutocompliter struct{}

func (s *stubSecondSuccessAutocompliter) FullTextSearh(query string) ([]interfaces.FullTextSearchItem, error) {
	return []interfaces.FullTextSearchItem{
		interfaces.FullTextSearchItem{"b", 2},
		interfaces.FullTextSearchItem{"d", 4},
	}, nil
}

type stubFailedAutocompliter struct{}

func (s *stubFailedAutocompliter) FullTextSearh(query string) ([]interfaces.FullTextSearchItem, error) {
	return nil, errExpected
}

func TestSuccess(t *testing.T) {
	expected := []string{"d", "c", "b", "a"}

	s := New(&stubFirstSuccessAutocompliter{}, &stubSecondSuccessAutocompliter{})
	actual, err := s.Autocomplete("foo")
	assert.DeepEqual(t, expected, actual)
	assert.Equal(t, nil, err)

	s = New(&stubSecondSuccessAutocompliter{}, &stubFirstSuccessAutocompliter{})
	actual, err = s.Autocomplete("foo")
	assert.DeepEqual(t, expected, actual)
	assert.Equal(t, nil, err)
}

func TestFailed(t *testing.T) {
	var expected []string
	s := New(&stubFailedAutocompliter{}, &stubSecondSuccessAutocompliter{})
	actual, err := s.Autocomplete("foo")
	assert.DeepEqual(t, expected, actual)
	assert.Equal(t, errExpected, err)

	s = New(&stubSecondSuccessAutocompliter{}, &stubFailedAutocompliter{})
	_, err = s.Autocomplete("foo")
	assert.DeepEqual(t, expected, actual)
	assert.Equal(t, errExpected, err)
}
