package autocomplete

import (
	"sort"
	"sync"

	"bitbucket.org/ieshmelev/theboats/interfaces"
)

type Service struct {
	models, builders interfaces.FullTextSearcher
}

func New(models, builders interfaces.FullTextSearcher) *Service {
	return &Service{
		models:   models,
		builders: builders,
	}
}

func (s *Service) Autocomplete(query string) ([]string, error) {

	wg := sync.WaitGroup{}

	var models []interfaces.FullTextSearchItem
	var modelsErr error
	wg.Add(1)
	go func() {
		defer wg.Done()
		models, modelsErr = s.models.FullTextSearh(query)
	}()

	var builders []interfaces.FullTextSearchItem
	var buildersErr error
	wg.Add(1)
	go func() {
		defer wg.Done()
		builders, buildersErr = s.builders.FullTextSearh(query)
	}()
	wg.Wait()

	if modelsErr != nil {
		return nil, modelsErr
	}
	if buildersErr != nil {
		return nil, buildersErr
	}

	itemsCount := len(models) + len(builders)
	items := make([]interfaces.FullTextSearchItem, 0, itemsCount)
	items = append(items, models...)
	items = append(items, builders...)
	sort.Sort(interfaces.FullTextSearchItemsByRank(items))

	names := map[string]struct{}{}
	result := make([]string, 0, itemsCount)
	for _, v := range items {
		if _, ok := names[v.Name]; ok {
			continue
		}
		result = append(result, v.Name)
		names[v.Name] = struct{}{}
	}
	return result, nil
}
