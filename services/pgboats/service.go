package pgboats

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	"bitbucket.org/ieshmelev/theboats/interfaces"
)

type Service struct {
	db     *sql.DB
	logger *log.Logger
}

func New(db *sql.DB, logger *log.Logger) *Service {
	return &Service{
		db:     db,
		logger: logger,
	}
}

func (s *Service) GetAllByModelIds(ids []int) ([]interfaces.Boat, error) {
	idsLen := len(ids)
	if idsLen == 0 {
		return nil, nil
	}
	whereParts := make([]string, idsLen)
	args := make([]interface{}, idsLen)
	for k, v := range ids {
		whereParts[k] = fmt.Sprintf("$%d", k+1)
		args[k] = v
	}
	rows, err := s.db.Query(
		fmt.Sprintf("SELECT * FROM boats WHERE model_id IN (%s)", strings.Join(whereParts, ",")),
		args...,
	)
	if err != nil {
		return nil, fmt.Errorf("boats GetAllByModelIds query error: %s", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			s.logger.Println(err)
		}
	}()

	result := []interfaces.Boat{}
	for rows.Next() {
		id, fleetID, modelID := 0, 0, 0
		if err := rows.Scan(&id, &fleetID, &modelID); err != nil {
			return nil, fmt.Errorf("boats GetAllByModelIds scan row error: %s", err)
		}
		result = append(result, interfaces.Boat{
			ID:      id,
			FleetID: fleetID,
			ModelID: modelID,
		})
	}

	return result, nil
}
