package main

import (
	"context"
	"database/sql"
	"log"
	"os"
	"path"

	_ "github.com/lib/pq"
)

type updater interface {
	up(tx *sql.Tx) error
	down(tx *sql.Tx) error
}

func buildDependencies() (*log.Logger, *sql.DB, []updater, error) {
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)

	cfg := newConfig()

	db, err := sql.Open("postgres", cfg.PGConnection)
	if err != nil {
		return logger, nil, nil, err
	}
	if err := db.Ping(); err != nil {
		return logger, nil, nil, err
	}

	updaters := []updater{
		newCSVUpdater(path.Join(cfg.SrcPath, "fleets.csv"), "fleets", 2, logger),
		newCSVUpdater(path.Join(cfg.SrcPath, "yacht_builders.csv"), "yacht_builders", 2, logger),
		newCSVUpdater(path.Join(cfg.SrcPath, "yacht_models.csv"), "yacht_models", 3, logger),
		newCSVUpdater(path.Join(cfg.SrcPath, "boats.csv"), "boats", 3, logger),
		newCSVUpdater(path.Join(cfg.SrcPath, "calendar.csv"), "calendar", 4, logger),
	}

	return logger, db, updaters, nil
}

func main() {
	logger, db, updaters, err := buildDependencies()
	if err != nil {
		logger.Fatal(err)
	}
	defer func() {
		if err := db.Close(); err != nil {
			logger.Println(err)
		}
	}()

	logger.Println("updating started")
	tx, err := db.BeginTx(context.Background(), &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		logger.Println(err)
		return
	}

	for i := len(updaters) - 1; i >= 0; i-- {
		if err := updaters[i].down(tx); err != nil {
			logger.Println(err)
			logger.Println("rolback")
			if err := tx.Rollback(); err != nil {
				logger.Println(err)
			}
			return
		}
	}

	for i := range updaters {
		if err := updaters[i].up(tx); err != nil {
			logger.Println(err)
			logger.Println("rolback")
			if err := tx.Rollback(); err != nil {
				logger.Println(err)
			}
			return
		}
	}

	if err := tx.Commit(); err != nil {
		logger.Println(err)
		return
	}
	logger.Println("updating finished")
}
