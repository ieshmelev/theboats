package main

import "os"

const (
	defaultPGConnection = "postgres://user:pass@localhost:5432/db?sslmode=disable"
	defaultSrcPath      = "/usr/src/data"
)

type config struct {
	PGConnection string
	SrcPath      string
}

func newConfig() *config {
	cfg := &config{
		PGConnection: defaultPGConnection,
		SrcPath:      defaultSrcPath,
	}

	if pgConnection := os.Getenv("PG_CONNECTION"); pgConnection != "" {
		cfg.PGConnection = pgConnection
	}

	if srcPath := os.Getenv("SRC_PATH"); srcPath != "" {
		cfg.SrcPath = srcPath
	}

	return cfg
}
