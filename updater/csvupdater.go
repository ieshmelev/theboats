package main

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strings"
)

const defaultBatchSize = 10000

type csvUpdater struct {
	table, csv             string
	fieldsCount, batchSize int
	logger                 *log.Logger
}

func newCSVUpdater(csv, table string, fieldsCount int, logger *log.Logger) *csvUpdater {
	return &csvUpdater{
		table:       table,
		csv:         csv,
		logger:      logger,
		fieldsCount: fieldsCount,
		batchSize:   defaultBatchSize,
	}
}

func (u *csvUpdater) up(tx *sql.Tx) error {
	u.logger.Printf("%s up...", u.table)

	records, err := u.readRecords()
	if err != nil {
		return fmt.Errorf("reading %s src failed: %s", u.table, err)
	}
	recordsCount := len(records)
	u.logger.Printf("%s read %d records\n", u.table, recordsCount)
	if recordsCount == 0 {
		return nil
	}

	batches := u.splitRecords(records)
	for i, v := range batches {
		if err := u.upBatch(tx, i, v); err != nil {
			return err
		}
	}

	u.logger.Printf("%s up success", u.table)
	return nil
}

func (u *csvUpdater) upBatch(tx *sql.Tx, batchIndex int, records [][]string) error {
	recordsCount := len(records)
	values := make([]string, 0, recordsCount)
	args := make([]interface{}, 0, recordsCount*u.fieldsCount)
	for recordIndex, record := range records {
		valuesParts := make([]string, 0, len(record))
		for i, v := range record {
			var arg *string
			if v != "" {
				arg = new(string)
				*arg = v
			}
			args = append(args, arg)
			valuesParts = append(valuesParts, fmt.Sprintf("$%d", recordIndex*u.fieldsCount+i+1))
		}
		values = append(values, fmt.Sprintf("(%s)", strings.Join(valuesParts, ", ")))
	}

	query := fmt.Sprintf("INSERT INTO %s VALUES %s", u.table, strings.Join(values, ", "))
	if _, err := tx.Exec(query, args...); err != nil {
		return fmt.Errorf("insert to %s failed: %s", u.table, err)
	}
	return nil
}

func (u *csvUpdater) down(tx *sql.Tx) error {
	u.logger.Printf("%s down...", u.table)
	if _, err := tx.Exec(fmt.Sprintf("DELETE FROM %s", u.table)); err != nil {
		return fmt.Errorf("delete from %s failed: %s", u.table, err)
	}
	u.logger.Printf("%s down success", u.table)
	return nil
}

func (u *csvUpdater) readRecords() ([][]string, error) {
	file, err := os.Open(u.csv)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := file.Close(); err != nil {
			u.logger.Println(err)
		}
	}()

	reader := csv.NewReader(file)

	// NOTE: в идеальном случае файл нужно считывать построчно с помощью reader.Read()
	// и отправлять каждую в канал, где записи будут накапливаться в пачку и при достижении
	// максимального размера пачки записи должны быть вставлены в БД и тд.
	// Но так как объемы данных очень маленькие и на мой взгляд данный способ более понятный
	// и поддерживаемый я решил не усложнять данный код.
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	if len(records) == 0 {
		return nil, nil
	}
	return records[1:], nil
}

func (u *csvUpdater) splitRecords(records [][]string) [][][]string {
	batches := [][][]string{}
	for len(records) > u.batchSize {
		batches = append(batches, records[:u.batchSize])
		records = records[u.batchSize:]
	}
	if len(records) > 0 {
		batches = append(batches, records)
	}
	return batches
}
