module bitbucket.org/ieshmelev/theboats

require (
	github.com/golang-migrate/migrate/v4 v4.7.0
	github.com/lib/pq v1.2.0
	gotest.tools v2.2.0+incompatible
)
